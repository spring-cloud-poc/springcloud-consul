package com.foreveross.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 使用Feign作为消费方与生产方的中间对象,可以不用连接url,直接声明要连接的服务器的名称即可
 * 由此对象可以完成负载均衡
 * @author Administrator
 *
 */

//@FeignClient("eureka-client")//声明接口
@FeignClient(name="consul-service")//
public interface FeignService {
	
	//@RequestMapping(value = "/hi",method=RequestMethod.GET) //连接url
	@RequestMapping(value="/hi",method=RequestMethod.GET)
	public String hello(@RequestParam("name") String name); //需要的参数 实际为Http协议的key=value形式

	@RequestMapping(value="/hello")
	public String test();
	
	/*@RequestMapping(value="/client",method=RequestMethod.GET)
	public String client();*/
}
