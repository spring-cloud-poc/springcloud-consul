package com.foreveross.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foreveross.service.FeignService;



/**
 * 控制层调用service层
 * @author Administrator
 *
 */
@RestController
public class FeignController {
	
	private static final Logger LOG = Logger.getLogger(FeignController.class.getName());
	
	@Autowired
	private FeignService feignService;
	
	@RequestMapping("/hello")
	public String hello(@RequestParam String name) {
		//LOG.log(Level.INFO,"feign has accepted a request......");
		String hello = feignService.hello(name);
		return hello;
	}
	
	/*@RequestMapping("/health")
	public String health() {
		return "health test";
	}*/
	
	@RequestMapping("/client")
	public String client() {
		return "i am client from feign....";
	}
	
	
	@RequestMapping("/test")
	public String test() {
		LOG.log(Level.INFO,"feign has accepted a request......");
		return feignService.test();
	}
}
