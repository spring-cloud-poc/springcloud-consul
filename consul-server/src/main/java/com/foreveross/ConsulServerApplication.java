package com.foreveross;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
/**
 * consul作为注册中心
 * consul是个C/S架构的注册中心，不同于eureka，无需我们自己开启注册中心，直接启动C/S架构的注册中心即可
 * 另外最好使用版本最高不要操作9.0.3的consul注册中心
 * 默认的端口是8500
 * @author Administrator
 *
 */

@SpringBootApplication
@EnableDiscoveryClient
public class ConsulServerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ConsulServerApplication.class, args);
	}
}
