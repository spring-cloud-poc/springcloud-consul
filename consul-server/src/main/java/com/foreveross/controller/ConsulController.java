package com.foreveross.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsulController {
	
	@RequestMapping("/hi")
	public String hi(@RequestParam("name") String name) {
		return "a response from consul's provider:"+name;
	}
	
	@Value("${server.port}")
	private String port;
	
	@RequestMapping("/hello")
	public String hello() {
		return "the server port is:"+port;
	}
}
