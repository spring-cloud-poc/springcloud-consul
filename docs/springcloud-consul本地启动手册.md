# springcloud-consul本地启动手册

##目录
- [开发环境](#开发环境)
- [步骤](#步骤)


##[开发环境](#)
| 工具 | 描述 
| --- | --- 
| JDK1.8以上  | java开发环境 
| Maven3.3以上  | 项目管理工具
| Consul 0.7.2  | 分布式开发注册中心
| Eclipse-oxgen  | java开发的IDE
| Git  | 项目版本管理工具


##步骤
1. 安装JDK    
    安装1.8以上的JDK,并配置好相应的环境变量，确保安装好JDK并且可用。  
2. 安装好Maven并配置好相应的环境变量。    
3. 下载安装[consul](https://www.consul.io/downloads.html), 推荐使用0.9.3及以下版本，在window系统下, cmd执行开启,命令 

	>consul agent -dev  

4. 安装Eclipse开发工具
5. 安装Git
6. 下载springcloud-consul源码（这里可使用git下载源码） 
    
    >git clone https://gitee.com/spring-cloud-poc/springcloud-consul.git
   
7. 使用Eclipse打开springcloud-consul,先开启consul-server，再开启consul-client  
    打开springcloud-consul后Eclipse会自动下载项目所需的依赖包，若没有下载成功，可以手动下载依赖包安装到本地的maven仓库。
8. 访问API查看是否成功：
http://localhost:4001/hello?name=abc
![](result.png)






 



    

 





