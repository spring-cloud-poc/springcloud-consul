# Consul介绍

##概述

>CONSUL是GOOGLE开源的一个使用GO语言开发的服务发现、配置管理中心服务。内置了服务注册与发现框 架、分布一致性协议实现、健康检查、KEY/VALUE存储、多数据中心方案，不再需要依赖其他工具（比如ZOOKEEPER等）。服务部署简单，只有一个可运行的二进制的包。每个节点都需要运行AGENT，他有两种运行模式SERVER和CLIENT。每个数据中心官方建议需要3或5个SERVER节点以保证数据安全，同时保证SERVER-LEADER的选举能够正确的进行。简而言之是一个基于C/S架构的注册中心。

##Consul架构
![](https://img-blog.csdn.net/20160827175251975?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQv/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center) 

##组成部分
### Agent
>Agent是长期运行在每个consul集群成员节点上守护进程。通过命令consul agent启动。Agent有client和server两种模式。由于每个节点都必须运行agent，所有节点要么是client要么是server。所有的Agent都可以可以调用DNS或HTTP API，并负责检查和维护服务同步


###CLIENT 节点 
>CLIENT表示CONSUL的CLIENT模式，就是客户端模式。是CONSUL节点的一种模式，这种模式下，所有注册到当前节点的服务会被转发到SERVER，本身是不持久化这些信息

>

###SERVER节点
>SERVER表示CONSUL的SERVER模式，表明这个CONSUL是个SERVER，这种模式下，功能和CLIENT都一样，唯一不同的是，它会把所有的信息持久化的本地，这样遇到故障，信息是可以被保留的。


###以上理解
>Agent为一个software,运行的模式有client与server两种

>client单单负责请求转发给server进行处理的服务,在一个consul-cluster中可以有多个client,无状态,不会持久化信息到本地

>server来进行请求处理与响应,server基于选举机制,一般推荐集群里面开3~5个,leader-server除了以上的功能外,对内有同步信息,健康检测等

#consul集群

|node|address|model|
|-|:-:|-:|
|node1|192.168.1.100|server|
|node2|192.168.1.101|server|
|node3|192.168.1.102|server|
|node4|192.168.1.103|client|

1. 开启consul的server模式,cmd命令

>consul agent -server -bootstrap-expect 1 -data-dir /tmp/consul -node=node1 -bind=192.168.1.100 -ui

>consul agent -server -bootstrap-expect 2 -data-dir /tmp/consul -join 192.168.1.100 -node=node2 -bind=192.168.1.101 -ui

>consul agent -server -bootstrap-expect 2 -data-dir /tmp/consul -join 192.168.1.100 -node=node3 -bind=192.168.1.102 -ui
		

2.开启consul的client模式，cmd命令
>consul agent -data-dir /tmp/consul -join 192.168.1.100 -node=node3 -bind=192.168.1.103

	释义：
		-bootstrap-expect 2（表示在两个集群中选择一个leader，改为1的话，那就是默认自己为leader）
		-server （表示agent是以服务器的方式进行启动）
		-bind=10.7.19.148（绑定的是本机的地址）
		-node=n1（表示启动时设置的节点名称，这里节点名为n1）
		-data-dir /tmp/consul（consul持久化信息的路径）
		-join （加入已有的集群当中）
		-ui (开启web管理界面)
**server模式官方推荐开启3-5个，一个作为leader，基于选举机制，当leader-server宕机后，会重新选举新的leader,但是client数量可以不限**

集群效果：

![](http://upload-images.jianshu.io/upload_images/4742055-d2660468207664d4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
